import requests as rq
import random
DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)
        
        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            ans = "None"
            if len(rj) == 3:
                ans = rj["answer"]
            right = rj["feedback"]
            print(right)
            print(ans)
            status = "win" in rj["message"]
            return ans, right, status
        self.reds = []
        choice = random.choice(self.choices)
        self.choices.remove(choice)
        ans, right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won and ans == "None":
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            ans, right, won = post(choice)
            tries.append(f'{choice}:{right}')
        if ans != "None":
            choice = ans
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))
    
    def get_feedback(self, guess: str) -> str:
        feedback = []
        for i in range(5):
            if guess[i] == self.secret_word[i]:
                feedback.append('G')
            elif guess[i] in self.secret_word:
                feedback.append('Y')
            else:
                feedback.append('R')
        return ''.join(feedback)
    
    def update(self, choice: str, right: int):
        for pos, color in enumerate(right):
            if color == "R":
                self.reds.append(choice[pos])
        def correct_letters(choice: str):
            return all(letter not in self.reds for letter in choice)            
        self.choices = [w for w in self.choices if correct_letters(w)]
        print(self.reds)
        print(len(self.choices))

game = MMBot("CodeShifu")
game.play()


